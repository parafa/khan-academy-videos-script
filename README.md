Khan Academy videos's copy utility
=======================

##Basic Usage
	$ python main.py -ch KhanAcademyEspanol -sf /source/folder -df /destination/folder
	
###Optional flags

	[-s]    Maximum size of collected videos's folder. In bytes
	[-qs]   Videos retrieved per query. 50 is the maximum and the default number.
	[-ql]   Maximum number of videos retrieved per execution. Leave it blank to retrieve all videos available. Usefull for testing purposes to limit the number of executed queries.