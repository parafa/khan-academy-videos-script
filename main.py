# coding: utf-8
import os
import logging
import argparse
from kafilechecker.khanacademy import VideoSelector

parser = argparse.ArgumentParser(description='Compare local videos with videos from a YouTube channel.')
logging.basicConfig(level=logging.INFO)

parser.add_argument( '-ch',
					dest='channel_name',
					metavar='channel_name',
					nargs='?',
					required=True,
					help='Youtube\'s channel name from which videos will be matched.')

parser.add_argument(  '-sf',
					dest='videos_source_folder',
					metavar='videos_source_folder',
					nargs='?',
					required=True,
					help='Videos\'s source folder.')

parser.add_argument(  '-df',
					dest='videos_destination_folder',
					metavar='videos_destination_folder',
					nargs='?',
					required=True,
					help='Videos\'s destination folder.')

parser.add_argument(  '-s',
					dest='size_in_bytes',
					metavar='size_in_bytes',
					type=int,
					nargs='?',
					help='Maximum size of collected videos\'s folder. In bytes')

parser.add_argument(  '-qs',
					dest='query_size',
					metavar='query_size',
					type=int,
					nargs='?',
					help='Videos retrieved per query. 50 is the maximum number')

parser.add_argument(  '-ql',
					dest='query_limit',
					metavar='query_limit',
					type=int,
					nargs='?',
					help='Maximum number of videos retrieved per execution. Leave it blank to retrieve all videos available.')

parser.add_argument( '-os',
					dest='only_with_subtitles',
					metavar='only_with_subtitles',
					nargs='?',
					help='Default: false. Set it to true if you just want to copy videos that have subtitles available.')

args = parser.parse_args()


#check if videos source folder is a valid folder
if os.path.isdir(args.videos_source_folder):
    pass
else:
    logging.error("Videos's source folder is not a valid directory")
    sys.exit(0);

#check if videos destination folder is a valid folder
if  os.path.isdir(args.videos_destination_folder):
    pass
else:
    logging.error("Videos's destination folder is not a valid directory")
    sys.exit(0);

vs = VideoSelector()

vs.channel_name = args.channel_name
vs.offline_videos_path = args.videos_source_folder
vs.videos_destination_folder = args.videos_destination_folder


if args.only_with_subtitles and args.only_with_subtitles == 'true':
	vs.only_with_subtitles = True

if args.size_in_bytes:
	vs.bytes_maximum_size = args.size_in_bytes

if args.query_size:
	vs.query_size = args.query_size

if args.query_limit:
	vs.query_limit = args.query_limit


vs.initialize()