import json
import os
import codecs
import urlparse
import urllib2
import logging
import xml.etree.ElementTree as ET
import json
import time

logging.basicConfig(level=logging.INFO)

class Videos:
    video_id = ""
    category = ""
    playlist = ""

class Category:
    title = ""
    subcategories = []
    query_index=1
    query_size=50    
    query_limit=-1
    query_counter=0
    videos = []
    json = dict()

    def init(self):
        #create master dict to hold the json will be written
        self.json = {'category':self.title, 'subcategories':[]}

        if self.subcategories:
            
            #create dict to hold each playlist
            playlist_with_videos = {}

            #iterate through playlists
            for s in self.subcategories:
                logging.info('Processing playlist: ' + s['title'])  
                
                #empty videos list for the next playlist
                self.videos = []

                #reset to 1, first start index for the next playlist
                self.query_index = 1

                #empty array that will hold the videos list for the next playlist
                playlist_with_videos = {}

                #call method to get the playlists/videos
                self.get_playlist_videos(s)

                #build object with all the videos found for this playlist
                playlist_with_videos = {"playlist":s['title'], "videos":self.videos}

                #store object with a subcategory and it's videos into the json object
                self.json['subcategories'].append(playlist_with_videos)

    def get_videos(self):
        return self.json

    def get_playlist_videos(self, playlist):
        time.sleep(2)
        #youtube refuses searches beyond 1000 items 
        if self.query_index > 1000:
            return

        #url to get all uploads from a single channel     
        uploads_endpoint = 'http://gdata.youtube.com/feeds/api/playlists/' + playlist['id'] + '?start-index=' + str(self.query_index) + '&max-results=' + str(self.query_size)
       
        try:
            #request to youtube api
            response = urllib2.urlopen(uploads_endpoint)

            #call method to proccess the request made to youtube api
            self.proccess_response(response, playlist)
            #logging.error('Working url: ' + uploads_endpoint)
        except Exception as e:
            #logging.error('Broken url: ' + uploads_endpoint)
            #logging.error(str(e))
            pass
        
        #var just to keep track of how many queries was executed at a time
        self.query_counter = self.query_counter + 1
        #logging.info("Queries executed: " + str(self.query_counter))
        
        #increase the query index following the query size
        self.query_index = self.query_index + self.query_size
        
        #check if query index has reached the maximum number for queries results
        if self.query_limit >= self.query_index:
            #logging.info("Query limit: " + str(self.query_limit) + " Query index: " + str(self.query_index))
            #call this same method recursively to get all videos until reach the limit
            self.get_playlist_videos(playlist);     

    def proccess_response(self, response, playlist):

        #parse response from youtube api into a xml tree
        tree = ET.parse(response)
        root = tree.getroot()

        #elementTree always appends the namespace related to the tag right before the tag name.
        xmlns = '{http://www.w3.org/2005/Atom}'
        medians ='{http://search.yahoo.com/mrss/}' 
        openSearchns = '{http://a9.com/-/spec/opensearchrss/1.0/}'
                
        #retrieve the total videos that this channel have. check it to make sure it will get only once per script execution
        if self.query_limit == -1:
            for totalResults in tree.findall(openSearchns + 'totalResults'):
                self.query_limit = int(totalResults.text)

        #logging.info('Processing videos for playlist: ' + playlist['title'])
        total_videos = 0

        for entry in tree.findall(xmlns+'entry'):

            try:
                video_id = ""
                video_title = ""

                for title in entry.findall(xmlns + 'title'):
                    video_title = title.text

                for group in entry.findall(medians + 'group'):

                    for player in group.findall(medians + 'player'):

                        url = player.attrib['url']

                        #extract the video id from the video url
                        url_parsed = urlparse.urlparse(url)
                        attr = urlparse.parse_qsl(url_parsed.query)
                        video_id = attr[0][1]

                # discard the element to free alocated memory
                entry.clear()
                total_videos += 1
                self.videos.append({"vid":video_id, "title":video_title})
            except:
                logging.info('Error')        
                pass
        
        logging.info('Videos processed: ' + str(total_videos))         

class CategoryManager:

    def init(self):

        math = Category()
        sciences = Category()
        economy = Category()
        history = Category()
        all_videos = []

        subjects = [math, sciences, economy, history]

        math.subcategories = [
            {"title": "Cryptography", "id": "PL7AEDF86AABA1AA9A"},
            {"title": "Exercise Module Videos", "id": "PLF763A77A7EE3429D"},
            {"title": "Developmental Math 3", "id": "PLE23E2FDF6E935778"},
            {"title": "Old Trignometry", "id": "PL33BB6C4FE847602F"},
            {"title": "Developmental Math 2", "id": "PL1C68557896CFABA8"},
            {"title": "Competition Math", "id": "PL7A6A47ED537EB925"},
            {"title": "IIT JEE Questions", "id": "PLA1B5F09DE551ECD5"},
            {"title": "Old Algebra", "id": "PL1824F04B0CEF5095"},
            {"title": "Old Algebra Videos", "id": "PLDF4D9FDDCD25EC2E"},
            {"title": "Developmental Math", "id": "PL50D1D09ABE9CE271"},
            {"title": "Algebra I Worked Examples", "id": "PL3128E15B8D159842"},
            {"title": "ck12.org Algebra 1 Examples", "id": "PL8956F31CD4DDA9FA"},
            {"title": "Statistics", "id": "PL1328115D3D8A2566"},
            {"title": "California Standards Test: Geometry", "id": "PLFE3A143757536D07"},
            {"title": "California Standards Test: Algebra I", "id": "PLDF7228D0F377BD07"},
            {"title": "California Standards Test: Algebra II", "id": "PL9DAFA5380375E44A"},
            {"title": "GMAT: Problem Solving", "id": "PL3EF88D8D529A562E"},
            {"title": "GMAT Data Sufficiency", "id": "PLCBA57C878A4A52D8"},
            {"title": "Differential Equations", "id": "PL96AE8D9C68FEB902"},
            {"title": "Linear Algebra", "id": "PLFD0EB975BA0CC1E0"},
            {"title": "Singapore Math", "id": "PLE32009038DDECA10"},
            {"title": "Probability", "id": "PLC58778F28211FA19"},
            {"title": "Geometry", "id": "PL26812DF9846578C3"},
            {"title": "Arithmetic", "id": "PL301908982CBFE20D"},
            {"title": "Pre-algebra", "id": "PL238F98B2C6422A95"},
            {"title": "Algebra", "id": "PL7AF1C14AF1B05894"},
            {"title": "Trigonometry", "id": "PLD6DA74C1DBF770E7"},
            {"title": "Precalculus", "id": "PLE88E3C9C7791BD2D"},
            {"title": "Calculus", "id": "PL19E79A0638C8D449"},
            {"title": "MA Tests for Education Licensure (MTEL) -Pre-Alg", "id": "PL6C6E2833B19F17AE"},
            {"title": "CAHSEE Example Problems", "id": "PL44D72BB7BED3A172"},
            {"title": "SAT Preparation", "id": "PL31AED425C1B489F6"}
        ]

        economy.subcategories = [
            {"title": "Microeconomics and Macroeconomics", "id": "PLAEA5E9ACA1508F92"},
            {"title": "Personal Finance", "id": "PL83DF21B47327EDFE"},
            {"title": "Currency", "id": "PLD2DC08AECFD90850"},
            {"title": "Valuation and Investing", "id": "PL370CD04C45C6BB81"},
            {"title": "Current Economics", "id": "PL3C1664BF6E9E3C8F"},
            {"title": "Venture Capital and Capital Markets", "id": "PLCADCB4565CFACEBF"},
            {"title": "Banking and Money", "id": "PLCECDA315A8848B99"},
            {"title": "Credit Crisis", "id": "PL945E4F0ED131E4D1"},
            {"title": "Finance", "id": "PL9ECA8AEB409B3E4F"},
            {"title": "Geithner Plan", "id": "PL4EF8BA0ADAAA1B05"},
            {"title": "Paulson Bailout", "id": "PLBE233FA3D593154E"}
        ]

        sciences.subcategories = [
            {"title": "Healthcare and Medicine", "id": "PL01AEFFF7C4C3C881"},
            {"title": "Computer Science", "id": "PL36E7A2B75028A3D6"},
            {"title": "Old Physics Videos", "id": "PL5CED85D3746F6127"},
            {"title": "Cosmology and Astronomy", "id": "PL2186CFB2CE12A8B5"},
            {"title": "Organic Chemistry", "id": "PL7305D1BC80498DA6"},
            {"title": "Biology", "id": "PL7A9646BC5110CF64"},
            {"title": "Chemistry", "id": "PL166048DD75B05C0D"},
            {"title": "Physics", "id": "PLAD5B880806EBE0A4"}
        ]

        history.subcategories = [
            {"title": "New and Noteworthy", "id": "PL20CEC80FE46830DE"},
            {"title": "American Civics", "id": "PL6C586B2226255BD8"},
            {"title": "History", "id": "PLAC6B9F15C835224C"}
        ]

        math.title = "Math"
        history.title = "History"
        sciences.title = "Sciences"
        economy.title = "Economy"

        for subject in subjects:
            subject.init()
            videos = subject.get_videos()
            all_videos.append(videos)

        try:
            self.write_json(all_videos)
        except:
            pass
        
        return all_videos

    def write_json(self, _json):    
        _json = json.dumps(_json, ensure_ascii=False, indent=1, default=lambda o: o.__dict__)
        fdir = os.path.join('all_videos.json')

        with codecs.open(fdir , 'w+', 'utf-8') as fi:
            os.chmod(fdir, 0777)
            fi.write(unicode(_json, encoding='utf-8'))
            fi.close()

        return True           

#cat = CategoryManager()
#cat.init()