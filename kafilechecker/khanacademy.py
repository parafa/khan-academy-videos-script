# coding: utf-8
import json
import codecs
import os
import sys
import shutil
import urllib2
import logging
from pycaption import CaptionConverter, SRTReader, WebVTTWriter
import xml.etree.ElementTree as ET
import category_manager

class Videos:

    video_id = ""
    view_count = 0
    filesize = 0
    online = False
    file_path = ""
    file_extension = ""
    caption_file_path = ""
    caption_file_extension = ".srt"
    category = ""
    subcategory = ""

class VideoSelector:


    #ARGS
    channel_name = ""
    videos_destination_folder = ""
    offline_videos_path=""

    videos_online = []
    videos_offline= []
    videos_youtube= []
    not_found_videos = []
    videos_copy_list = []
    videos_copy_list_with_subtitles = []
    query_index=1
    query_size=50
    query_limit=-1
    query_counter=0
    ignore_less_visualized_videos=True
    ignore_less_visualized_than=10
    bytes_maximum_size = 1073741824
    captions_folder_name = "captions"
    only_with_subtitles = False

    def __init__(self):
        logging.basicConfig(level=logging.INFO)
        self.current_folder = os.path.dirname(os.path.abspath(__file__))

    def initialize(self):

        #get all mp4 videos available on a directory
        self.get_offline_videos()

        #get all videos uploaded to a youtube channel
        self.pick_videos_from_channel()

        #cross reference the list of online and offline videos
        self.cross_videos()

        #copy offline videos based on one criterea: most viewed. Copy until pre-stablished limit has been reached
        self.copy_most_viewed_videos()

        #get all videos found in the khan academy's playlists and cross with the found videos to categorize them.
        c = category_manager.CategoryManager()
        self.videos_youtube = c.init()
        json = self.categorize_most_viewed_videos()

        #write json with all videos categorized
        self.write_json(json)

        #remove videos not found on youtube
        self.clean_not_found_videos(self.not_found_videos)


    def categorize_most_viewed_videos(self):

        json = []

        for category in self.videos_youtube:
            category_node = dict()
            category_node["has_subcategories"]  = "true"
            category_node["machine_name"]       = category["category"].lower()
            category_node["playlist"]           = []
            category_node["subcategories"]      = []

            for subcategory in category['subcategories']:
                subcategory_node = dict()
                subcategory_node['subject'] = subcategory['playlist']
                subcategory_node['playlist'] = []

                for video in subcategory['videos']:

                    if self.only_with_subtitles == True:
                        copy = self.search_video_in_array(self.videos_copy_list_with_subtitles, video['vid'])
                    else:
                        copy = self.search_video_in_array(self.videos_copy_list, video['vid'])

                    logging.info("Searching video: " + video['vid'])

                    if copy != False:
                        video_node = dict()
                        video_node['status'] = "0"
                        video_node['title'] = video['title']
                        video_node['vid'] = video['vid']
                        #category_node["subcategories"][index]["playlist"].append(video_node)
                        subcategory_node['playlist'].append(video_node)
                        logging.info("Keeping video: " + video['vid'])
                    else:
                        self.not_found_videos.append(video['vid'])


                #if playlist contain videos, add it to the category node
                if len(subcategory_node['playlist']) > 0:
                    category_node["subcategories"].append(subcategory_node)



            json.append(category_node)

        return json

    def write_json(self, _json):
        _json = json.dumps(_json, ensure_ascii=False, indent=1, default=lambda o: o.__dict__)
        fdir = os.path.join(self.videos_destination_folder, 'offline.json')

        with codecs.open(fdir , 'w+', 'utf-8') as fi:
            os.chmod(fdir, 0777)
            fi.write(unicode(_json, encoding='utf-8'))
            fi.close()

        return True

    def clean_not_found_videos(self, list):
        try:
            for file in list:
                filename = os.path.join(self.videos_destination_folder, str(file) + ".mp4")
                os.remove(filename)
                logging.info("removing video: " + str(file))
        except:
            pass

    def pick_videos_from_channel(self):
        #url to get all uploads from a single channel
        uploads_endpoint = 'http://gdata.youtube.com/feeds/api/users/' + self.channel_name + '/uploads?start-index=' + str(self.query_index) + '&max-results=' + str(self.query_size)

        #request to youtube api
        response = urllib2.urlopen(uploads_endpoint)

        #var just to keep track of how many queries was executed at a time
        self.query_counter = self.query_counter + 1
        logging.info("Queries executed: " + str(self.query_counter))

        #call method to proccess the request made to youtube api
        self.proccess_response(response)

        #increase the query index following the query size
        self.query_index = self.query_index + self.query_size

        #check if query index has reached the maximum number for queries results
        if self.query_limit >= self.query_index:
            logging.info("Query limit: " + str(self.query_limit) + " Query index: " + str(self.query_index))
            #call this same method recursively to get all videos until reach the limit
            self.pick_videos_from_channel();


    def proccess_response(self, response):

        #parse response from youtube api into a xml tree
        tree = ET.parse(response)

        #elementTree always appends the namespace related to the tag right before the tag name.
        xmlns = '{http://www.w3.org/2005/Atom}'
        ytns = "{http://gdata.youtube.com/schemas/2007}"
        openSearchns= "{http://a9.com/-/spec/opensearchrss/1.0/}"

        #retrieve the total videos that this channel have. check it to make sure it will get only once per script execution
        if self.query_limit == -1:
            for totalResults in tree.findall(openSearchns + 'totalResults'):
                self.query_limit = int(totalResults.text)

        for entry in tree.findall(xmlns + 'entry'):
            #create instance of video to hold all informations
            v = Videos()
            v.online = True

            for vid in entry.findall(xmlns + 'id'):
                #extract the video id from the video url
                clean_id = vid.text.split("/")
                clean_id = clean_id[len(clean_id)-1]
                v.video_id = clean_id

            for count in entry.findall(ytns + 'statistics'):
                v.view_count = int(count.get('viewCount'))
            # discard the element to free alocated memory
            entry.clear()
            if self.ignore_less_visualized_videos == False:
                self.videos_online.append(v)
            elif self.ignore_less_visualized_videos == True and v.view_count < self.ignore_less_visualized_than:
                pass
            else:
                self.videos_online.append(v)


    def get_offline_videos(self):

        #walk throught the videos dir
        for root, dirs, files in os.walk(self.offline_videos_path):

            #iterate throught the files inside the dir
            for f in files:

                file_extension = os.path.splitext(f)[1]

                #check if the file is a mp4 video. if not, skip to the next file
                if file_extension != ".mp4":
                    continue


                #create fullpath to the file
                fullpath = os.path.join(root, f)
                v = Videos()

                v.file_extension = file_extension
                v.file_path = fullpath
                #get file's filesize
                v.filesize = os.stat(fullpath).st_size

                #extract video id which is the filename without the extension
                v.video_id = os.path.splitext(f)[0]
                logging.info("Processing video: " + v.video_id)
                self.get_offline_captions(v)
                self.videos_offline.append(v)

    def get_offline_captions(self, video):
        #walk throught the videos dir
        for root, dirs, files in os.walk(self.offline_videos_path):

            #iterate throught the files inside the dir
            for f in files:

                file_extension = os.path.splitext(f)[1]

                #extract video id which is the filename without the extension
                filename = os.path.splitext(f)[0]

                #check if the file is a mp4 video. if not, skip to the next file
                if file_extension != ".srt":
                    continue

                if video.video_id != filename:
                    continue

                logging.info("Caption found: " + video.video_id)
                #create fullpath to the file
                video.caption_file_path = os.path.join(root, f)


    def cross_videos(self):
        non_existent_videos = []

        #iterate throught all offile video objects
        for off_video in self.videos_offline:

            #look if a single offline video is in the online videos's array.
            video = self.search_video_in_array(self.videos_online, off_video.video_id)

            # if video is available, get the view count to sort later.
            if(video != False):
                off_video.view_count = video.view_count

        #sort all items in
        self.sort_by_view_count(self.videos_offline)


    def search_video_in_array(self, array, video_id):
        for video in array:
            if video.video_id == video_id:
                return video
        return False


    def sort_by_view_count(self, entries):
        entries.sort(key=lambda item:item.view_count, reverse=True)
        return entries


    def copy_most_viewed_videos(self):
        self.create_copy_list()

        if not os.path.exists(self.videos_destination_folder):
            os.makedirs(self.videos_destination_folder)


        #check if inside the videos folder there's a folder for the captions.
        if not os.path.exists(os.path.join(self.videos_destination_folder, self.captions_folder_name)):
            os.makedirs(os.path.join(self.videos_destination_folder, self.captions_folder_name))

        for video in self.videos_copy_list:
            logging.info("Copying video: " + video.video_id + " | Filesize: " + str(video.filesize) + " | View Count: " + str(video.view_count) )

            #copy videos
            filename = video.video_id + video.file_extension
            if self.only_with_subtitles == False:
                shutil.copy2(video.file_path, os.path.join(self.videos_destination_folder, filename))

            #check if video has caption. if yes, copy it.
            if video.caption_file_path != "":
                subtitle_filename = video.video_id + ".vtt"
                logging.info("Converting caption: " + video.video_id)
                subtitle_converted = self.convert_caption(video.caption_file_path, os.path.join(self.videos_destination_folder, subtitle_filename))

                if self.only_with_subtitles == True:
                    if subtitle_converted == True:
                        self.videos_copy_list_with_subtitles.append(video)
                        shutil.copy2(video.file_path, os.path.join(self.videos_destination_folder, filename))
                    else:
                        continue



    def convert_caption(self, source_caption, destination_caption):

        with open(source_caption) as srt_file:
            srt_caption = srt_file.read()
            srt_caption = unicode(srt_caption, "utf-8")
            srt_file.close()


        try:
            converter = CaptionConverter()
            converter.read(srt_caption, SRTReader())
        except:
            logging.info("Caption convertion failed")
            return False


        try:
            vtt_caption = converter.write(WebVTTWriter())

            vtt_file = open(destination_caption, "w")
            vtt_file.write(vtt_caption)
            vtt_file.close()
        except:
            logging.info("Caption convertion failed")
            return False

        return True


    #method to check which videos will be copied based on the maximum size stablished by the var bytes_maximum_size
    def create_copy_list(self):

        current_pos = 0

        #iterate through offline videos to sum its filesizes and find out which videos are to be copied
        for video in self.videos_offline:

            #check if the current file filesize plus current_pos are lower or equal than the maximum limit. if not, the loop has reached it's limit and will stop.
            if (video.filesize +  current_pos) <= self.bytes_maximum_size:

                if self.only_with_subtitles == True and video.caption_file_path == "":
                    continue

                #increase the current_pos count
                current_pos = current_pos + video.filesize

                #append video to a list of videos to be copied
                self.videos_copy_list.append(video)
            else:
                break
